class AppRoutes {
  AppRoutes._();

  /// Modul splash
  static const String splash = '/';

  /// Modul login google
  static const String login = '/login';

  /// Modul login kta
  static const String kta = '/kta';

  /// Modul registrasi
  static const String registrasi = '/registrasi';
}

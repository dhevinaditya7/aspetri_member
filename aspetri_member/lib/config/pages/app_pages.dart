import 'package:aspetri_member/core/kta/controller/kta_controller.dart';
import 'package:aspetri_member/core/kta/ui/kta_page_view.dart';
import 'package:aspetri_member/core/login/controller/login_controller.dart';
import 'package:aspetri_member/core/login/ui/login_page_view.dart';
import 'package:aspetri_member/core/registrasi_user/controller/registrasi_user_controller.dart';
import 'package:aspetri_member/core/registrasi_user/ui/registrasi_user.view.dart';
import 'package:aspetri_member/core/splash/controller/splash_controller.dart';
import 'package:aspetri_member/core/splash/ui/splash_page_view.dart';
import 'package:get/get.dart';

import '../routes/app_routes.dart';

class AppPages {
  AppPages._();

  static List<GetPage> getPages() {
    return [
      /// Modul splash
      GetPage(
        name: AppRoutes.splash,
        page: () => const SplashPageView(),
        binding: BindingsBuilder(() {
          Get.put(SplashController());
        }),
      ),

      /// Modul login
      GetPage(
        name: AppRoutes.login,
        page: () => const LoginPageView(),
        binding: BindingsBuilder(() {
          Get.lazyPut(() => LoginController());
        }),
      ),

      // modul KTA
      GetPage(
        name: AppRoutes.kta,
        page: () => const KtaPageView(),
        binding: BindingsBuilder(() {
          Get.lazyPut(() => KtaController());
        }),
      ),

      // modul registrasi user
      GetPage(
        name: AppRoutes.registrasi,
        page: () => const RegistrasiUserView(),
        binding: BindingsBuilder(() {
          Get.lazyPut(() => RegistrasiUserController());
        }),
      ),
    ];
  }
}

import 'package:aspetri_member/constant/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class TextFieldCustom extends StatelessWidget {
  final TextEditingController controller;
  final String? hintText;
  final String? labelText;
  final Widget? prefixIcon;
  final TextStyle? style;
  final TextInputType? keyboardType;
  final bool? obsecure;
  final int? maxLine;
  final int? maxLenght;
  final Color? outlineColors;
  final List<TextInputFormatter>? inputFormatters;
  final ValueChanged<String>? onChanged;
  final FormFieldValidator<String>? validator;

  const TextFieldCustom(
      {Key? key,
      required this.controller,
      this.hintText,
      this.labelText,
      this.keyboardType,
      this.style,
      this.obsecure,
      this.inputFormatters,
      this.onChanged,
      this.validator,
      this.prefixIcon,
      this.outlineColors,
      this.maxLenght,
      this.maxLine})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      maxLength: maxLenght,
      maxLines: maxLine,
      style: style,
      keyboardType: keyboardType,
      decoration: InputDecoration(
        prefixIcon: prefixIcon,
        labelText: labelText ?? "",
        labelStyle: Get.textTheme.bodySmall,
        hintText: hintText ?? "",
        hintStyle: Get.textTheme.titleMedium!.copyWith(
          color: Colours.hitam.withOpacity(0.25),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.r),
          borderSide:
              BorderSide(color: outlineColors ?? Colours.blue.withOpacity(0.8)),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.r),
          borderSide:
              BorderSide(color: outlineColors ?? Colours.blue.withOpacity(0.8)),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.r),
          borderSide:
              BorderSide(color: outlineColors ?? Colours.blue.withOpacity(0.8)),
        ),
        contentPadding: const EdgeInsets.fromLTRB(20, 16, 20, 16),
      ),
      obscureText: obsecure ?? false,
      validator: validator,
    );
  }
}

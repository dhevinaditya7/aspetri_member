import 'package:flutter/material.dart';

class Colours {
  Colours._();

  static const Color meblue = Color(0xFF3944BC);
  static const Color biru = Color(0xFF51C5D0);
  static const Color hitam = Color(0xFF222222);
  static const Color darkblue = Color(0xFF2990FF);
  static const Color greybgcolor = Color(0xFFF4F4F4);
  static const Color greyicon = Color(0xFF88888D);
  static const Color orangetext = Color(0xFFFFB000);
  static const Color cyantext = Color(0xFF00C6CE);
  static const Color greytext = Color(0xFF666666);
  static const Color ijo = Color(0xFF057E05);
  static const Color batal = Color(0xFFE5E7E9);
  static const Color themeColor = Color(0xfff5a623);
  static const Color primaryColor = Color(0xff203152);
  static const Color greyColor = Color(0xffaeaeae);
  static const Color greyColor2 = Color(0xffE8E8E8);
  static const Color blue = Color(0xFF428BFE);
  static const Color dark = Color(0xFF202020);
  static const Color dark1 = Color(0xFF111111);
  static const Color darkGrey = Color(0xFF575757);
  static const Color darkGrey2 = Color(0xFF2B2B2B);
  static const Color darkGrey3 = Color(0xFF15162B);
  static const Color darkPurple = Color(0xFF0C183D);
  static const Color darkPurple2 = Color(0xFF1D1F3D);
  static const Color darkPurple3 = Color(0xFF191E3D);
  static const Color darkRed = Color(0xFFC73C3C);
  static const Color green = Color(0xFF1AA89E);
  static const Color green2 = Color(0xFF158179);
  static const Color green3 = Color(0xFF4D9E99);
  static const Color greenLight = Color(0xFFD4EAF5);
  static const Color bgGrey = Color(0xFFF5F5F5);
  static const Color grey = Color(0xFF808080);
  static const Color grey1 = Color.fromARGB(255, 233, 233, 233);
  static const Color grey2 = Color(0xFFE1E1E1);
  static const Color grey3 = Color(0xFFD3D3D3);
  static const Color grey4 = Color(0xFFA9A9A9);
  static const Color light = Color(0xFFF8F8F8);
  static const Color light2 = Color(0xFFFAFAFA);
  static const Color light3 = Color(0xFFF1F1F1);
  static const Color lightBlue = Color(0xFF6E72B7);
  static const Color lightBlue2 = Color(0xFFB6B8D1);
  static const Color lightPurple = Color(0x1A4E53A4);
  static const Color lightestPurple = Color(0xFFE7E8FF);
  static const Color orange = Color(0xFFEDAB00);
  static const Color purple = Color(0xFF4E53A4);
  static const Color purple2 = Color(0xFF4A55A5);
  static const Color purple3 = Color(0xFFEFF2F7);
  static const Color red = Color(0xFFF02626);
  static const Color red2 = Color(0xFFFF4444);
  static const Color red3 = Color(0xFFF44336);
  static const Color yellow = Color(0xFFFC4E4E);
  static const Color yellow2 = Color(0xFFEDAB00);

  static const LinearGradient gradientDarkPurpleGreen = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [Colours.darkPurple, Colours.purple, Colours.green],
  );
  static const LinearGradient gradientPurpleGreen = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [Colours.purple, Colours.green],
  );

  static const LinearGradient gradientPurpleLightBlue = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [Colours.purple, Colours.lightBlue],
  );

  static const LinearGradient gradientYellow = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [Colours.yellow, Colours.yellow2],
  );

  static const LinearGradient gradientGreen = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [Colours.green2, Colours.green],
  );

  static const LinearGradient gradientGreen2 = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [Colours.green2, Colours.green3],
  );
  static const LinearGradient gradientRed = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [Colours.darkRed, Colours.red2],
  );
}

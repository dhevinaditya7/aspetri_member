import 'package:flutter/material.dart';

class AppConst {
  AppConst._();

  // app information
  static const String appName = "ASPETRI MEMBER";
  static String appVersion = '';

  static const String email = '';

  // app theme
  static const Size designSize = Size(428, 926);
}

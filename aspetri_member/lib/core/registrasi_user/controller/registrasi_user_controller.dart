import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class RegistrasiUserController extends GetxController {
  late TextEditingController namaC;
  late TextEditingController no_hpC;
  late TextEditingController no_ktaC;
  late TextEditingController alamatC;
  late TextEditingController provinsiC;
  late TextEditingController kabupatenC;
  late TextEditingController kecamatanC;
  late TextEditingController saldo_tahunanC;
  late TextEditingController saldo_EventC;

  FirebaseFirestore usrFirestore = FirebaseFirestore.instance;

  void addUsers(
      String nama,
      String no_hp,
      String no_KTA,
      bool status,
      String alamat,
      String provinsi,
      String kabupaten,
      String kecamatan,
      int saldo_Tahunan,
      int saldo_Event) {
    CollectionReference users = usrFirestore.collection("Users");

    try {
      users.doc(no_KTA).set({
        "nama": nama,
        "no_hp": no_hp,
        "no_kta": no_KTA,
        "status": status,
        "alamat": alamat,
        "provinsi": provinsi,
        "kabupaten": kabupaten,
        "kecamatan": kecamatan,
        "saldo_tahunan": saldo_Tahunan,
        "saldo_event": saldo_Event,
      });

      Get.defaultDialog(
          title: "Berhasil",
          middleText: "Berhasil Menambahkan Anggota",
          onConfirm: () {
            namaC.clear();
            no_hpC.clear();
            no_ktaC.clear();
            alamatC.clear();
            provinsiC.clear();
            kabupatenC.clear();
            kecamatanC.clear();
            saldo_tahunanC.clear();
            saldo_EventC.clear();
            Get.back();
          },
          textConfirm: "Okay");
    } catch (e) {
      print(e);
      Get.defaultDialog(
          title: "Terjadi Kesalahan",
          middleText: "Tidak Berhasil Menambahkan Anggota!");
    }
  }

  @override
  void onInit() {
    namaC = TextEditingController();
    no_hpC = TextEditingController();
    no_ktaC = TextEditingController();
    alamatC = TextEditingController();
    provinsiC = TextEditingController();
    kabupatenC = TextEditingController();
    kecamatanC = TextEditingController();
    saldo_tahunanC = TextEditingController();
    saldo_EventC = TextEditingController();
    super.onInit();
  }

  @override
  void onClose() {
    namaC.dispose();
    no_hpC.dispose();
    no_ktaC.dispose();
    alamatC.dispose();
    provinsiC.dispose();
    kabupatenC.dispose();
    kecamatanC.dispose();
    saldo_tahunanC.dispose();
    saldo_EventC.dispose();
    super.onClose();
  }
}

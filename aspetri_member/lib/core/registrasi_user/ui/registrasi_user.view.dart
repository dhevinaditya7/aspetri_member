import 'package:aspetri_member/core/registrasi_user/controller/registrasi_user_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegistrasiUserView extends StatelessWidget {
  const RegistrasiUserView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int saldoTahunan = 0;
    int saldoEvent = 0;

    final controller = Get.find<RegistrasiUserController>();
    return Scaffold(
      appBar: AppBar(
        title: Text("Registrasi User"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            TextField(
              controller: controller.namaC,
              autocorrect: false,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(labelText: "Nama User"),
            ),
            TextField(
              controller: controller.no_hpC,
              autocorrect: false,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(labelText: "Nomor Hp"),
            ),
            TextField(
              controller: controller.no_ktaC,
              autocorrect: false,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(labelText: "Nomor Kta"),
            ),
            TextField(
              controller: controller.alamatC,
              autocorrect: false,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(labelText: "Alamat"),
            ),
            TextField(
              controller: controller.provinsiC,
              autocorrect: false,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(labelText: "Provinsi"),
            ),
            TextField(
              controller: controller.kabupatenC,
              autocorrect: false,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(labelText: "Kabupaten"),
            ),
            TextField(
              controller: controller.kecamatanC,
              autocorrect: false,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(labelText: "Kecamatan"),
            ),
            TextField(
              controller: controller.saldo_tahunanC,
              autocorrect: false,
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(labelText: "Saldo Tahunan"),
            ),
            TextField(
              controller: controller.saldo_EventC,
              autocorrect: false,
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(labelText: "Saldo Event"),
            ),
            ElevatedButton(
                onPressed: () => controller.addUsers(
                      controller.namaC.text,
                      controller.no_hpC.text,
                      controller.no_ktaC.text,
                      false,
                      controller.alamatC.text,
                      controller.provinsiC.text,
                      controller.kabupatenC.text,
                      controller.kecamatanC.text,
                      saldoTahunan = int.parse(controller.saldo_tahunanC.text),
                      saldoEvent = int.parse(controller.saldo_EventC.text),
                    ),
                child: Text("Tambahkan User"))
          ],
        ),
      ),
    );
  }
}

import 'package:aspetri_member/constant/colors.dart';
import 'package:aspetri_member/core/kta/components/card_input_kta.dart';
import 'package:aspetri_member/core/kta/components/heading_kta_page.dart';
import 'package:aspetri_member/core/kta/components/privacy_police_text.dart';
import 'package:aspetri_member/core/kta/controller/kta_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class KtaPageView extends StatelessWidget {
  const KtaPageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<KtaController>();
    return Scaffold(
        backgroundColor: Colours.bgGrey,
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                // Heading
                HeadingKtaPage(),

                // input email
                CardKta(
                  lanjutkan: () => controller.lanjutkan(),
                  noKtaController: controller.noKtaController,
                ),

                // terms and condition
                TextSkdanPrivacy()
              ],
            ),
          ),
        ));
  }
}

// ignore_for_file: prefer_const_constructors

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TextSkdanPrivacy extends StatelessWidget {
  const TextSkdanPrivacy({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 28.h),
      child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
              style: TextStyle(color: Colors.black, fontSize: 14),
              children: <TextSpan>[
                TextSpan(
                    text:
                        'Dengan menjadi pengguna berarti Anda telah setuju dengan '),
                TextSpan(
                  text: 'Syarat dan Ketentuan ',
                  style: TextStyle(color: Colors.blue),
                  recognizer: TapGestureRecognizer()..onTap = () => () {},
                ),
                TextSpan(text: '& '),
                TextSpan(
                  text: 'Kebijakan Privasi ',
                  style: TextStyle(color: Colors.blue),
                  recognizer: TapGestureRecognizer()..onTap = () => () {},
                ),
                TextSpan(text: 'kami ')
              ])),
    );
  }
}

// ignore_for_file: prefer_const_constructors

import 'package:aspetri_member/constant/colors.dart';
import 'package:aspetri_member/widget/text_field_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CardKta extends StatelessWidget {
  final TextEditingController noKtaController;
  final VoidCallback lanjutkan;
  const CardKta(
      {Key? key, required this.lanjutkan, required this.noKtaController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200.h,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(6.r),
        boxShadow: [
          BoxShadow(
              offset: const Offset(0, 2),
              color: Colors.grey.withOpacity(0.10),
              blurRadius: 8.r)
        ],
      ),
      width: MediaQuery.of(context).size.width - 54.w,
      padding: const EdgeInsets.all(14),
      child: Column(
        children: [
          Row(
            children: [
              SizedBox(
                height: 60.h,
                width: 240.w,
                child: TextFieldCustom(
                  controller: noKtaController,
                  maxLenght: 12,
                  maxLine: 1,
                  hintText: "Nomor KTA",
                  prefixIcon: Padding(
                    padding: EdgeInsets.fromLTRB(0, 12.h, 0, 0),
                    child: Text(
                      'ASP ',
                      style: TextStyle(color: Colours.dark, fontSize: 20.h),
                    ),
                  ),
                  outlineColors: Colors.transparent,
                  style: TextStyle(color: Colours.dark, fontSize: 20.h),
                ),
              ),
            ],
          ),
          SizedBox(height: 10.h),
          GestureDetector(
            onTap: lanjutkan,
            child: Container(
              padding: EdgeInsets.all(16),
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.r),
                color: Colors.green,
                boxShadow: [
                  BoxShadow(
                      offset: Offset(0, 3),
                      color: Colors.black.withOpacity(0.20),
                      blurRadius: 8)
                ],
              ),
              child: Text("Lanjutkan",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white)),
            ),
          )
        ],
      ),
    );
  }
}

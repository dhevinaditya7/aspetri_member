import 'package:aspetri_member/core/splash/components/logo_app.dart';
import 'package:flutter/material.dart';

class SplashPageView extends StatelessWidget {
  const SplashPageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        /// Logo Splash <-
        child: LogoApp(),
      ),
    );
  }
}

import 'package:get/get.dart';

class SplashController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    cekUserLogin();
  }

  cekUserLogin() async {
    Future.delayed(const Duration(seconds: 2), () {
      Get.toNamed('/login');
    });
  }
}

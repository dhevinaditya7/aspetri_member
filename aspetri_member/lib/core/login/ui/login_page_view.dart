// ignore_for_file: prefer_const_constructors

import 'package:aspetri_member/constant/colors.dart';
import 'package:aspetri_member/core/login/components/card_input_email.dart';
import 'package:aspetri_member/core/login/components/heading_login_page.dart';
import 'package:aspetri_member/core/login/components/privacy_police_text.dart';
import 'package:aspetri_member/core/login/controller/login_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginPageView extends StatelessWidget {
  const LoginPageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<LoginController>();
    return Scaffold(
        backgroundColor: Colours.bgGrey,
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                // Heading
                HeadingLoginPage(),

                // input email
                CardEmail(
                  masukDenganGoogle: () => controller.googleButton(),
                ),

                // terms and condition
                TextSkdanPrivacy()
              ],
            ),
          ),
        ));
  }
}

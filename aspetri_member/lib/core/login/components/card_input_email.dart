// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CardEmail extends StatelessWidget {
  final VoidCallback masukDenganGoogle;
  const CardEmail({Key? key, required this.masukDenganGoogle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200.h,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(6.r),
        boxShadow: [
          BoxShadow(
              offset: const Offset(0, 2),
              color: Colors.grey.withOpacity(0.10),
              blurRadius: 8.r)
        ],
      ),
      width: MediaQuery.of(context).size.width - 54.w,
      padding: const EdgeInsets.all(14),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(
            onPressed: masukDenganGoogle,
            style: ElevatedButton.styleFrom(primary: Colors.green),
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 50.w,
                    child: Image.asset("assets/images/google.png"),
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  Text(
                    "Masuk dengan akun Google",
                    style: TextStyle(color: Colors.white),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

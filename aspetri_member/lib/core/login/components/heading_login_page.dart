import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HeadingLoginPage extends StatelessWidget {
  const HeadingLoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image.asset(
          "assets/images/logo.png",
          height: 120.h,
        ),
        SizedBox(height: 80.h),
        Text("Selamat Datang",
            style: TextStyle(fontSize: 18.h, fontWeight: FontWeight.w600)),
        SizedBox(height: 20.h),
        Text("Silahkan input email anda",
            style: TextStyle(
              fontSize: 16.h,
            )),
        SizedBox(height: 32.h),
      ],
    );
  }
}
